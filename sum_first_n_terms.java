public class NthSeries {
	
	public static String seriesSum(int n) {
		  // Happy Coding ^_^	
    
      double sum = 0.0;
      double denom = 1.0;
      
      for (int i = 0; i < n; i++) {
          sum += 1.0 / denom;
          denom += 3.0;
      }
    
      return String.format("%.2f", sum);
	}
}